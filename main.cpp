/******************************************************************

 n-gram index character based search

 @author Paul Bottin <paul.bottin+git@gmail.com>
 @file   main.cpp

******************************************************************/

#include "ngram.h"
#include "csvreader.h"

#include <string>
#include <iomanip>
#include <cstdlib>

using namespace std;
using namespace csvio;
using namespace search;

typedef ngram_index<4> ngram_index_type;

// create the index by reading in the CSV file and adding all words from the second column
void create_index(CSVReader& csv, ngram_index_type& index)
{
  string column;
  CSVReader::ReadResult status;
  bool okay = true;
  size_t col = 0;
  size_t words = 0;

  while (okay)
  {
    // iterate over each column
    status = csv.read(column);
    switch (status)
    {
    // reader returned IO Error
    case CSVReader::READ_ERROR:
      cout << "Error reading CSV" << endl;
      okay = false;
      ++col;
      break;

    // reader reached end of file
    case CSVReader::READ_END:
      cout << "Reached end of CSV" << endl;
      okay = false;
      ++col;
      break;

    // reader just read a column
    case CSVReader::READ_COLUMN:
      ++col;
      break;

    // reader just read the first column in a row
    case CSVReader::READ_LINE:
      col = 0;
      break;
    }

    // filter out interesting columns
    if (col == 2)
    {
      // add the word from column 2 to the search index
      index.add_word_to_index(column);
      ++words;

      // every 1000 words print how far we got
      if ((words % 1000) == 0)
      {
        cout << "words: " << words << endl;
      }
    }

  }

}

// application main entry point
int main()
{
  // word dictionary
  vector<string> dict;

  // word index
  ngram_index_type index(dict);

  // open the csv file and create the index...
  CSVReader csv("cities15000.txt", "\t", "\n", "");
  create_index(csv, index);

  // output the number of ngrams in the index
  cout << "num " << ngram_index_type::NGRAM_SIZE << "-grams:  " << index.size() << endl;

  for (;;)
  {
    // input
    string eingabe;
    cout << "Eingabe: ";
    getline(cin, eingabe);
    cout << endl;

    // processing the lookup
    list<ngram_index_type::lookup_word_result > result;
    eingabe = ngram_index_type::trim_all(index.lookup_word(eingabe, result), "_");

    // limit the number of results
    size_t n = 25;

    // output result table header
    cout << "        "
         << setw(64) << "WORD" << " "
         << setw(8)  << "COUNT" << " "
         << setw(12) << "DICE" << endl;

    // output result table body...
    for (list<ngram_index_type::lookup_word_result >::iterator it = result.begin(); it != result.end(); it++)
    {
      --n;
      if (!n)
        break;
      cout << "result: "
           << setw(64) << it->m_word << " "
           << setw(8)  << it->m_numOccurences << " "
           << setw(12) << it->m_diceCoeff << endl;
    }
    cout << endl;

  }

  return 0;
}
