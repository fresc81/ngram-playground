/******************************************************************

 comma separated value file/stream reader

 @author Paul Bottin <paul.bottin+git@gmail.com>
 @file   csvreader.h

******************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>

#include <string>
#include <vector>
#include <list>
#include <map>
#include <set>

#include <stdint.h>


namespace csvio
{

/**
 * a class for reading a separated values file
 * the class can be configured to read different kinds of files:
 *   comma separated, colon separated, tab separated, with or without string delimiter chars, etc.
 */
class CSVReader
{
public:
  // possible return values when reading the csv
  enum ReadResult
  {
    READ_ERROR = -1,
    READ_END,
    READ_LINE,
    READ_COLUMN
  };

private:
  typedef std::set<char>           chars_t;
  typedef std::vector<std::string> strings_t;

  // input streeam
  bool           m_managed; // stream is opened/closed by this object
  std::istream * m_stream;  // pointer to output stream

  // columns buffer
  std::string    m_buffer;

  // file offset of the current line
  off_t          m_offset;
  bool           m_newline;

  // delimiter strings converted to sets
  const chars_t  m_column_delim;
  const chars_t  m_line_delim;
  const chars_t  m_string_delim;

public:
  /**
   * construct a csv reader that reads from a stream
   *   the default setting reads files whose columns are separated by (,) or (;)
   *   and whose lines are separated by a newline or a null character
   *   and whose strings are delimited by (") or (')
   */
  CSVReader(
    std::istream& stream = std::cin,
    const std::string& column_delim = ",;",
    const std::string& line_delim   = "\n\0",
    const std::string& string_delim = "\"'"
  ):
    m_managed(false),
    m_stream(&stream),
    m_buffer(),
    m_offset(0),
    m_newline(true),
    m_column_delim(column_delim.begin(), column_delim.end()),
    m_line_delim(line_delim.begin(), line_delim.end()),
    m_string_delim(string_delim.begin(), string_delim.end())
  {
  }

  /**
   * constructs a csv reader that reads from a file given as filename
   */
  CSVReader(
    const std::string& filename,
    const std::string& column_delim = ",;",
    const std::string& line_delim   = "\n\0",
    const std::string& string_delim = "\"'"
  ):
    m_managed(true),
    m_stream(new std::ifstream(filename.c_str())),
    m_buffer(),
    m_offset(0),
    m_newline(true),
    m_column_delim(column_delim.begin(), column_delim.end()),
    m_line_delim(line_delim.begin(), line_delim.end()),
    m_string_delim(string_delim.begin(), string_delim.end())
  {
  }

  /**
   * destroys a csv reader
   */
  virtual ~CSVReader()
  {
    if (m_managed)
      delete m_stream;
  }

  /**
   * returns a const reference to the underlying stream
   */
  inline const std::istream& stream() const
  {
    return *m_stream;
  }

  /**
   * returns a reference to the underlying stream
   */
  inline std::istream& stream()
  {
    return *m_stream;
  }

  /**
   * returns the offset of the beginning of the current row within the file
   */
  inline const off_t& offset() const
  {
    return m_offset;
  }

  /**
   * read a single column from the file
   */
  ReadResult read(std::string& data)
  {
    bool is_string = false;
    char string_sep = '\0';
    int ch;

    if (stream().eof())
      return READ_END;

    if (stream().bad())
      return READ_ERROR;

    for (;;)
    {
      ch = stream().get();

      if (stream().eof())
      {
        data = m_buffer;
        m_buffer.clear();
        if (m_newline)
        {
          m_newline = false;
          return READ_LINE;
        }
        return READ_COLUMN;
      }

      if (stream().bad())
      {
        data = m_buffer;
        m_buffer.clear();
        if (m_newline)
        {
          m_newline = false;
          return READ_LINE;
        }
        return READ_COLUMN;
      }

      if (is_string) // has been opened
      {
        if ((char)ch == string_sep) // separators match
        {

          is_string = false; // close string
          continue; // proceed to next char

        }
      }

      if (!is_string && m_string_delim.count((char)ch)) // if ch is in string_delim
      {

        // string
        string_sep = (char)ch;
        is_string = true;

      } else
      if (!is_string && m_column_delim.count((char)ch)) // if ch is in column_delim
      {

        // column
        data = m_buffer;
        m_buffer.clear();

        if (m_newline)
        {
          m_newline = false;
          return READ_LINE;
        }
        return READ_COLUMN;

      } else
      if (!is_string && m_line_delim.count((char)ch)) // if ch is in line_delim
      {

        // line
        data = m_buffer;
        m_buffer.clear();
        m_offset = stream().tellg();

        if (m_newline)
        {
          return READ_LINE;
        }

        m_newline = true;
        return READ_COLUMN;

      } else // is some other character
      {

        // other
        m_buffer += (char)ch;

      }
    }

    // never reached
    return READ_ERROR;
  }

};

}
