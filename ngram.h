/******************************************************************

 n-gram index

 @note   needs to be compiled with -std=gnu++11 or -std=c++11
         other compilers then gcc-4.8 may or may not work

 @author Paul Bottin <paul.bottin+git@gmail.com>
 @file   ngram.h

******************************************************************/

#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <list>
#include <map>
#include <set>

#include <cctype>
#include <cmath>

namespace search
{

  // ngram index
  template <const size_t N, typename TokenT = char, typename WordT = std::basic_string<TokenT> >
  struct ngram_index
  {

    // number of tokens that make up the ngram
    enum
    {
      NGRAM_SIZE = N
    };

    // token type (usually char)
    typedef TokenT                                    token_type;

    // ngram is a static array of N tokens
    typedef std::array<token_type, NGRAM_SIZE>        ngram_type;

    // todo create text_type / idea: no separator chars -> character based search -> else word based search

    // word type (usually std::string)
    typedef WordT                                     word_type;

    // vector of words (each word is referred to by it's index)
    typedef std::vector<word_type>                    vector_word_type;

    // type of index used with vector (usually 32 bit unsigned int)
    typedef typename vector_word_type::size_type      index_type;

    // linked list of ngrams
    typedef std::list<ngram_type>                     list_ngram_type;

    // treeset of ngrams
    typedef std::set<ngram_type>                      set_ngram_type;

    // treemap of an word's index mapped it's number of occurences in the index
    typedef std::map<index_type, size_t>              map_index_size_type;

    // treemap of an ngram to it's map of occurences
    typedef std::map<ngram_type, map_index_size_type> map_ngram_map_index_size_type;

    // a pair of a word's index and it's number of found ngrams in the index
    typedef std::pair<index_type, size_t>             pair_index_size_type;

    // a list of above pairs
    typedef std::list<pair_index_size_type>           list_pair_index_size_type;

    // an item returned from querying the index
    struct lookup_word_result
    {

      word_type  m_word;
      index_type m_index;
      double     m_diceCoeff;
      size_t     m_numOccurences;

      lookup_word_result(const word_type& word, const index_type& index, double diceCoeff, size_t numOccurences):
        m_word(word),
        m_index(index),
        m_diceCoeff(diceCoeff),
        m_numOccurences(numOccurences)
      {
      }

      lookup_word_result(const lookup_word_result& other):
        m_word(other.m_word),
        m_index(other.m_index),
        m_diceCoeff(other.m_diceCoeff),
        m_numOccurences(other.m_numOccurences)
      {
      }

    };

    // a list of lookup result items
    typedef std::list<lookup_word_result>             list_lookup_word_result_type;

  private:

    const word_type               m_underscore;  // a string of underscore chars

    vector_word_type&             m_dictionary;  // assigns a unique number to each word

    map_ngram_map_index_size_type m_ngram_index; // the actual search index, modelled using STL containers

  public:
    // construct by passing a reference to the word dictionary, which assigns a unique index number to each word
    ngram_index(vector_word_type& dict):
      m_underscore(NGRAM_SIZE-1, '_'),
      m_dictionary(dict),
      m_ngram_index()
    {
    }

    // converts ngrams represented by a string of N chars to an array with N chars
    inline static ngram_type word_to_ngram(const word_type& word)
    {
      ngram_type result;
      for (size_t i=0; i<N; i++)
      {
        result[i] = word[i];
      }
      return result;
    }

    // converts ngrams represented by an array with N chars to a string containing N chars
    inline static word_type ngram_to_word(const ngram_type& ngram)
    {
      word_type result(N, '_');
      for (size_t i=0; i<N; i++)
      {
        result[i] = ngram[i];
      }
      return result;
    }

    // returns a set of ngrams which contains only elements available in both given collections
    template <typename InputIteratorT>
    inline static set_ngram_type join_ngrams(
      const InputIteratorT& a1,
      const InputIteratorT& a2,
      const InputIteratorT& b1,
      const InputIteratorT& b2
    )
    {
      set_ngram_type result;

      for (InputIteratorT i = a1; i != a2; i++)
      {
        for (InputIteratorT j = b1; j != b2; j++)
        {
          if (*i == *j)
            result.insert(*i);
        }
      }

      return result;
    }

    // comparator: compares if the number of occurences is 0
    inline static bool test_pair_index_size_null_size(const pair_index_size_type& a)
    {
      // compare pair of index and frequency by frequency
      return a.second == 0;
    }

    // comparator: compare a pair of index and size by it's index
    inline static int compare_pair_index_size_by_index(const pair_index_size_type& a, const pair_index_size_type& b)
    {
      // compare pair of index and frequency by index
      return 0 > ((long) a.first - (long) b.first);
    }

    // sort the given list by the index component of their elements
    inline static void sort_list_pair_index_size_by_index(list_pair_index_size_type& pairs)
    {
      // sort the result of get_occurences()
      pairs.sort(compare_pair_index_size_by_index);
    }

    // compare the lookup result items by their dice coefficient (which denotes how narrow the word is related to the searched word)
    inline static bool compare_lookup_word_result_by_dice(const lookup_word_result& a, const lookup_word_result& b)
    {
      // compare pair of index and frequency by frequency
      return a.m_diceCoeff > b.m_diceCoeff;
    }

    // sort the lookup result list by the dice coefficient of the elements
    inline static void sort_lookup_word_result_by_dice(list_lookup_word_result_type& results)
    {
      // sort the result of get_occurences()
      results.sort(compare_lookup_word_result_by_dice);
    }

    // compresses the list of found indices by removing duplicates and counting the number of occurences
    inline static void compress_list_pair_index_size(list_pair_index_size_type& pairs)
    {
      // sort by index
      sort_list_pair_index_size_by_index(pairs);

      // eliminate duplicate indices
      typename list_pair_index_size_type::iterator previous;
      typename list_pair_index_size_type::iterator it;
      for (previous = pairs.begin(), it = previous, ++it; it != pairs.end(); previous = it, ++it)
      {
        if (it->first == previous->first) // same index
        {
          it->second += previous->second; // add frequency
          previous->second = 0;
        }
      }

      // remove pairs with second == 0
      pairs.remove_if(test_pair_index_size_null_size);

    }

    // helper method that replaces 'what' in 'word' with 'repl'
    inline static word_type replace_all(const word_type& word, const word_type& what, const word_type repl)
    {
      word_type result(word);
      typename word_type::size_type i;
      bool changed;

      do
      {
        changed = false;

        i = result.find(what);
        if (i != word_type::npos)
        {
          changed = true;
          result.replace(i, what.length(), repl);
        }

      } while (changed);

      return result;
    }

    // helper method that trims all leading and trailing chars that occur in 'token' from 'word'
    inline static word_type trim_all(const word_type& word, const word_type& token)
    {
      typename word_type::size_type a = word.find_first_not_of(token);
      typename word_type::size_type b = word.find_last_not_of(token)+1;
      return ((a!=word_type::npos) && (b!=word_type::npos) && (a<b)) ? word.substr(a, b-a) : word;
    }

    // lowercases the given word, pre- and suffixes it with underscores and replace space characters by underscores
    inline static word_type lower_remove_spaces(const word_type& word)
    {
      word_type result;

      for (typename word_type::const_iterator it = word.begin(); it != word.end(); it++)
        if (std::isalpha(*it))
          result += std::tolower(*it); // lowercased alphabet
        else if (std::isdigit(*it))
          result += *it; // digits allowed
        else
          result += '_'; // everything else becomes underscore

      // singleton underscores, trimmed at begin/end
      return trim_all(replace_all(result, "__", "_"), "_");
    }

    // calculate the dice coefficient for the two given words
    inline double dice_coeff(const word_type& a, const word_type& b)
    {
      list_ngram_type ta;
      extract_word_ngrams(a, ta);

      list_ngram_type tb;
      extract_word_ngrams(b, tb);

      set_ngram_type tab = join_ngrams(ta.begin(), ta.end(), tb.begin(), tb.end());
      return 2.0*double(tab.size()) / (double(ta.size())+double(tb.size()));
    }

    // return the number of ngrams in the index
    inline size_t size() const
    {
      return m_ngram_index.size();
    }

    // extract a list of ngrams from the word and store the result in 'ngrams'
    word_type extract_word_ngrams(const word_type& word, list_ngram_type& ngrams)
    {
      // create __word__
      const word_type underscored = m_underscore + lower_remove_spaces(word) + m_underscore;

      // is empty string
      if (underscored.length() <= 2*m_underscore.length())
        return underscored;

      // std::cout << "extract: " << underscored << std::endl;

      // get all ngram permutations
      word_type buffer;
      for (size_t i = NGRAM_SIZE-1; i < underscored.length(); i++)
      {
        buffer = "";
        for (size_t j = 0; j < NGRAM_SIZE; j++)
        {
          buffer = underscored[i-j] + buffer;
        }

        // std::cout << "ngram: " << buffer << std::endl;
        ngrams.push_back(word_to_ngram(buffer));
      }
      return underscored;
    }

    // gets a list of index/frequency pairs that match the given ngram and store them to 'result'
    void get_occurences(const ngram_type& ngram, list_pair_index_size_type& result)
    {
      // get occurences of an ngram in the index
      map_index_size_type& occurences = m_ngram_index[ngram];
      for (typename map_index_size_type::iterator it = occurences.begin(); it != occurences.end(); it++)
      {
        result.push_back(*it);
      }
    }

    // adds the given word to the index and return the normalized word as it is indexed
    word_type add_word_to_index(const word_type& word)
    {
      // index word
      size_t i;
      typename vector_word_type::iterator it;

      // find word in index
      for (i = 0, it = m_dictionary.begin(); it != m_dictionary.end(); i++, it++)
      {
        if (word == *it) // word already in index
          break;
      }

      if (it == m_dictionary.end()) // did not find word
      {
        // add new word to index
        i = m_dictionary.size();
        m_dictionary.push_back(word);
      }

      // i is now a unique unsigned int assigned to word

      // extract and count ngram occurences
      list_ngram_type ngrams;
      word_type result = extract_word_ngrams(word, ngrams);
      for (typename list_ngram_type::iterator nit = ngrams.begin(); nit != ngrams.end(); nit++)
      {
        m_ngram_index[*nit][i] ++;
      }
      return result;
    }

    // lookup the given word in the index and store the matches to 'result', also return the normalized word as it would be indexed
    word_type lookup_word(const word_type& word, list_lookup_word_result_type& result)
    {
      // get word ngrams
      list_ngram_type ngrams;
      word_type underscored = extract_word_ngrams(word, ngrams);
      list_pair_index_size_type temp;

      // for each ngram in word
      for (typename list_ngram_type::iterator nit = ngrams.begin(); nit != ngrams.end(); nit++)
      {
        // get indices from index
        get_occurences(*nit, temp);

        // compress same indices
        compress_list_pair_index_size(temp);
      }

      // for each index, extract word, it's number of occurences and calculate the dice coefficient
      for (typename list_pair_index_size_type::iterator iit = temp.begin(); iit != temp.end(); iit++)
      {
        word_type& found = m_dictionary[iit->first];
        result.push_back(lookup_word_result(found, iit->first, dice_coeff(word, found), iit->second));
      }

      // sort result list by dice coefficient
      sort_lookup_word_result_by_dice(result);

      return underscored;
    }

  };

}
